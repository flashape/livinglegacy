// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

// apiRoot:'http://localhost:5000/living-legacy/us-central1/api/'

export const environment = {
  production: false,
  apiRoot:'https://us-central1-living-legacy-ab939.cloudfunctions.net/api/'
};
