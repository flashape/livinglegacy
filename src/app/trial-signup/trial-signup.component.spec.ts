import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TrialSignupComponent } from './trial-signup.component';

describe('TrialSignupComponent', () => {
  let component: TrialSignupComponent;
  let fixture: ComponentFixture<TrialSignupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TrialSignupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TrialSignupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
