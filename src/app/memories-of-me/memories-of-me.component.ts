import { Component, OnInit } from '@angular/core';
import {AuthService} from '../auth.service';
import {AngularFirestore, AngularFirestoreCollection} from 'angularfire2/firestore';
import {StarterQuestion} from '../model/starter-question';
import {Observable} from 'rxjs/Observable';
import {Post} from '../model/post';
import {PostsService} from '../posts.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-memories-of-me',
  templateUrl: './memories-of-me.component.html',
  styleUrls: ['./memories-of-me.component.css']
})
export class MemoriesOfMeComponent implements OnInit {
  questionsCollection: AngularFirestoreCollection<StarterQuestion>;
  starterQuestions: Observable<StarterQuestion[]>;

  constructor(private authService:AuthService, private afs: AngularFirestore, private postsService: PostsService, private router:Router) { }

  ngOnInit() {
    this.questionsCollection = this.afs.collection('starterQuestions');
    this.starterQuestions = this.questionsCollection.snapshotChanges().map(actions => {
      return actions.map(action => {
        const data = action.payload.doc.data() as StarterQuestion;
        const id = action.payload.doc.id;

        let q: StarterQuestion = new StarterQuestion();
        q.id = id;
        q.text = data.text;
        console.log(q)
        return q;
      });
    });

  }

  starterQuestionAnswered(question: StarterQuestion): boolean {
    return this.authService.userInfo.starterQuestionsAnswered[question.id];
  }
  getPostFromStartQuestionId(question: StarterQuestion): string {
    return this.authService.userInfo.starterQuestionsAnswered[question.id];
  }

  answerQuestion(question:StarterQuestion){

    if(this.starterQuestionAnswered(question)){
      console.log("starter question has been ansered, in post : ", this.getPostFromStartQuestionId(question));
      this.router.navigate(['/editPost', this.getPostFromStartQuestionId(question)] );

    }else{

      // prepare new post with uid
      // copy question id to new post

      let newPost: Post = this.postsService.create();
      newPost.starterQuestionId = question.id;
      newPost.title = question.text;

      //save post draft first, then navigate to edit post

      this.postsService.save(newPost)
        .then(()=>{
          console.log("POST "+ newPost.id+"DRAFT SAVED, going to edit post screen...")
          this.postsService.pendingPost = newPost;
          this.router.navigate(['createPost']);
        })
        .catch((error)=>{
          console.log("ERROR SAVING POST DRAFT :");
          console.error(error);
          debugger;
        });
    }


  }

}
