import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MemoriesOfMeComponent } from './memories-of-me.component';

describe('MemoriesOfMeComponent', () => {
  let component: MemoriesOfMeComponent;
  let fixture: ComponentFixture<MemoriesOfMeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MemoriesOfMeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MemoriesOfMeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
