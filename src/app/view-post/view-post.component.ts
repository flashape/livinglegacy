import {Component, ElementRef, OnInit, ViewChild, AfterViewInit} from '@angular/core';
import {Carousel} from 'primeng/primeng';
import {Post} from "../model/post";
import {AngularFirestore, AngularFirestoreCollection} from "angularfire2/firestore";
import {Observable} from "rxjs/Observable";
import {ActivatedRoute, ActivatedRouteSnapshot, Router} from "@angular/router";
import {PostsService} from "../posts.service";
import {AuthService} from "../auth.service";
import {FirebaseApp} from "angularfire2";
import {ConfirmationService} from "primeng/api";
import {SwiperConfigInterface} from "ngx-swiper-wrapper";

@Component({
  selector: 'app-view-post',
  templateUrl: './view-post.component.html',
  styleUrls: ['./view-post.component.css']
})
export class ViewPostComponent implements OnInit {


  private _post:Post;

  get post(): Post {
    return this._post;
  }

  set post(value: Post) {
    this._post = value;
  }

  swiperConfig: SwiperConfigInterface = {
    direction: 'horizontal',
    slidesPerView: 'auto',
    keyboard: true,
    centeredSlides: true,
    autoHeight: true
  };

  photosCollection: AngularFirestoreCollection<any>;
  photos: Observable<any[]>;

  constructor(    private elementRef: ElementRef,
                  public postsService: PostsService,
                  private authService: AuthService,
                  private firebaseApp: FirebaseApp,
                  private afs:AngularFirestore,
                  private router:Router,
                  private confirmationService:ConfirmationService,
                  private route: ActivatedRoute ) {

  }

  @ViewChild('carousel') carousel:Carousel;

  ngOnInit() {
    let snapshot:ActivatedRouteSnapshot = this.route.snapshot;
    console.log("snapshot : ", snapshot);

    this.route.data
      .subscribe((data) => {
        console.log(data)
        this.post = data.post;
      });


    this.photosCollection = this.afs.collection(this.postsService.getMediaItemsPath(this._post));

    this.photos = this.photosCollection.snapshotChanges().map(actions => {
      return actions.map(action => {
        const data = action.payload.doc.data();
        const id = action.payload.doc.id;
        //
        // let q: StarterQuestion = new StarterQuestion();
        data.id = id;
        data.editingCaption = false;
        // q.text = data.text;
        console.log("photo : ", data);

        return data;
      });
    });
  }

  ngAfterViewInit(){
    console.log('carousel : ', this.carousel)
  }

  onPreviousClick(){
    this.carousel.onPrevNav();
  }

  onNextClick(){
    this.carousel.onNextNav()
  }

}
