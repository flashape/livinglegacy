import {SafeUrl} from '@angular/platform-browser';

export class TempFile {
  url: SafeUrl;
  type: string;
  file?: File;
  arrayBuffer: ArrayBuffer;
  caption?: string;
  blob?:Blob; // if the content was loaded from storage we will have a blob not a file object
  mimeType?:string = "image/jpg"
  name?:string;

}
