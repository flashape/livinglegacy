import { Component, OnInit } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {AuthService} from '../auth.service';
import {Router} from '@angular/router';
import {TrialService} from '../trial.service';

declare var StripeCheckout: any;
@Component({
  selector: 'app-pricing',
  templateUrl: './pricing.component.html',
  styleUrls: ['./pricing.component.css']
})
export class PricingComponent implements OnInit {

  constructor(private http: HttpClient, private authService: AuthService, private router: Router, private trialService: TrialService) { }

  stripeHandler;
  showLogin = false;
  showCompletingPurchase = false;
  showCompletePurchaseWait = false;
  showCompletePurchaseSuccess = false;
  showCompletePurchaseError = false;

  showSaveTrialData = false;
  showContinueAfterSuccess = false;

  purchaseErrorMessage:string;
  ngOnInit() {
  }



  getJsonHeaderOption(): any{
    const httpOptions = {};

    let headers = new HttpHeaders({
      'Content-Type': 'application/json'
    });

    httpOptions['headers'] = headers;

    return httpOptions;
  }



  selectPlan(event, planType){

    if(!this.authService.authenticated){
      this.showLogin = true;
      return;
    }

    switch(planType){

      case 'standard':


        this.stripeHandler = StripeCheckout.configure({
          key: 'pk_test_Prqay0GufT6dUyH3cNnHtGPn',
          image: '/assets/img/living-legacy-icon.png',
          locale: 'auto',
          email:this.authService.user.email,
          token: (token) => {
            // You can access the token ID with `token.id`.
            // Get the token ID to your server-side code for use.
            this.onSubscriptionToken(token);
          }
        });


        this.stripeHandler.open({
          name: 'Living Legacy',
          description: 'Annual Subscription',
          amount: 1900
        });



        break;

      case 'forever-storage':

        this.stripeHandler = StripeCheckout.configure({
          key: 'pk_test_Prqay0GufT6dUyH3cNnHtGPn',
          locale: 'auto',
          image: '/assets/img/living-legacy-icon.png',
          email:this.authService.user.email,

          token: (token) => {
            // You can access the token ID with `token.id`.
            // Get the token ID to your server-side code for use.
            this.onForeverToken(token);
          }
        });

        this.stripeHandler.open({
          name: 'Living Legacy',
          description: '50 GB Plan',
          amount: 9900
        });


        break;



      case 'forever-storage-eternal':

        this.stripeHandler = StripeCheckout.configure({
          key: 'pk_test_Prqay0GufT6dUyH3cNnHtGPn',
          locale: 'auto',
          image: '/assets/img/living-legacy-icon.png',
          email:this.authService.user.email,
          token: (token) => {
            // You can access the token ID with `token.id`.
            // Get the token ID to your server-side code for use.
            this.onUnlimitedToken(token);
          }
        });


        this.stripeHandler.open({
          name: 'Living Legacy',
          description: 'Unlimited GB Plan',
          amount: 19900
        });


        break;


    }



    // standard
    // forever-storage
    // forever-storage-eternal

    event.preventDefault();

    // Close Checkout on page navigation:
    window.addEventListener('popstate', () => {
      this.stripeHandler.close();
    });

  }

  createOrder(sku){

    let httpOptions = this.getJsonHeaderOption();

    let endpoint = environment.apiRoot+'order'
    this.http.post(endpoint, {sku:sku, customerId:'cus_CO38xhKlgRFvTt'}, httpOptions )
      .subscribe(
        (result)=>{
          console.log("success creating order : ", result);

        },
        (error)=>{
          console.log("ERROR creating order : ", error);
        }
      )
  }





  onSubscriptionToken(token){
    console.log("subscription token : ", token);

    this.showCompletingPurchase = true;
    this.showCompletePurchaseWait = true;


    let httpOptions = this.getJsonHeaderOption();


    let endpoint = environment.apiRoot+'subscription'

    console.log("SUBSCRIPTION endpoint : ", endpoint);

    this.http.post(endpoint, {token: token}, httpOptions )
      .subscribe(
        (result)=>{
          console.log("success purchasing subscription : ", result);
          this.showCompletePurchaseWait = false;
          this.showCompletePurchaseSuccess = true;

          if(this.trialService.hasLocalSavedData){
            this.showSaveTrialData = true;
          }else{
            this.showContinueAfterSuccess = true;
          }

        },
        (error)=>{
          console.log("ERROR purchasing subscription : ", error);
          this.showCompletePurchaseWait = false;
          this.showCompletePurchaseError = true;
          this.purchaseErrorMessage = error.error.message;
        }
      )
  }


  onForeverToken(token){
    console.log("forever token : ", token);

    this.showCompletingPurchase = true;
    this.showCompletePurchaseWait = true;


    let httpOptions = this.getJsonHeaderOption();


    let endpoint = environment.apiRoot+'membership'
    this.http.post(endpoint, {token: token, sku: 'sku_CO4zWs6zzNYnZ7'}, httpOptions )
      .subscribe(
        (result)=>{
          console.log("success purchasing forever membership : ", result);
          this.showCompletePurchaseWait = false;
          this.showCompletePurchaseSuccess = true;

          if(this.trialService.hasLocalSavedData){
            this.showSaveTrialData = true;
          }else{
            this.showContinueAfterSuccess = true;
          }

        },
        (error)=>{
          console.log("ERROR purchasing forever membership : ", error);
          this.showCompletePurchaseWait = false;
          this.showCompletePurchaseError = true;
          this.purchaseErrorMessage = error.error.message;
        }
      )
  }


  onUnlimitedToken(token){
    console.log("unlimitd token : ", token);
    this.showCompletingPurchase = true;
    this.showCompletePurchaseWait = true;

    let httpOptions = this.getJsonHeaderOption();


    let endpoint = environment.apiRoot+'membership'
    this.http.post(endpoint, {token: token, sku: 'sku_CO4zJkgjmhibta'}, httpOptions )
      .subscribe(
        (result)=>{
          console.log("success purchasing eternal membership : ", result);
          this.showCompletePurchaseWait = false;
          this.showCompletePurchaseSuccess = true;

          if(this.trialService.hasLocalSavedData){
            this.showSaveTrialData = true;
          }else{
            this.showContinueAfterSuccess = true;
          }

        },
        (error)=>{
          console.log("ERROR purchasing eternal membership : ", error);
          this.showCompletePurchaseWait = false;
          this.showCompletePurchaseError = true;
          this.purchaseErrorMessage = error.error.message;
        }
      )
  }

  onStartSharingClick(){
    this.router.navigate(['questions']);
  }

  onGoToMyAccountClick(){
    this.router.navigate(['my-account']);

  }


  onLoginSuccess(){
      this.showLogin = false;
  }

  onCreateAccountSuccess(){
    this.showLogin = false;
  }

  closeError(){
    this.showCompletePurchaseError = false;
    this.showCompletingPurchase = false;

  }

  onSaveTrialDataClick(){
    this.trialService.saveComplete.subscribe((post)=>{
      this.router.navigate(['user', this.authService.userId, 'post', post.id]);
    })


    this.trialService.saveTrialData();
  }

  onDiscardTrialDataClick(){
    this.trialService.discard();
    this.showCompletePurchaseSuccess = true;
    this.showSaveTrialData = false;
    this.showContinueAfterSuccess = true;

  }
}
