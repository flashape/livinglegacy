import {Post} from './model/post';

import { Injectable } from '@angular/core';
import {Resolve, ActivatedRouteSnapshot, Router} from '@angular/router';
import {PostsService} from './posts.service';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/take';

@Injectable()
export class PostResolver implements Resolve<Post> {

  constructor(private postsService: PostsService, private router:Router) {}

  resolve(route: ActivatedRouteSnapshot, ) {
    console.log("PostREsolver.resolve() id : ", route.paramMap.get('postId'));

    // return this.cs.getCrisis(id).take(1).map(crisis => {
    //   if (crisis) {
    //     return crisis;
    //   } else { // id not found
    //     this.router.navigate(['/crisis-center']);
    //     return null;
    //   }
    // });


    return this.postsService.getPost(route.paramMap.get('postId')).take(1).map(post => {
      console.log("returning post : ", post);
      if (post) {
        return post;
      } else { // id not found
        this.router.navigate(['/not-found']);
        return null;
      }
    });
  }
}
