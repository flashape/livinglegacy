import { Component, OnInit } from '@angular/core';
import {AuthService} from "../auth.service";
import {AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument} from 'angularfire2/firestore';
import {StarterQuestion} from '../model/starter-question';
import {Observable} from 'rxjs/Observable';
import {Post} from '../model/post';
import {PostsService} from '../posts.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-vault',
  templateUrl: './vault.component.html',
  styleUrls: ['./vault.component.css']
})
export class VaultComponent implements OnInit {

  questionsCollection: AngularFirestoreCollection<StarterQuestion>;
  starterQuestions: Observable<StarterQuestion[]>;

  postsCollection: AngularFirestoreCollection<Post>;
  posts: Observable<Post[]>;


  constructor(public authService: AuthService, private afs: AngularFirestore, private postsService: PostsService, private router:Router) { }

  ngOnInit() {

    this.questionsCollection = this.afs.collection('starterQuestions');
    this.postsCollection = this.afs.collection(this.postsService.getUserPostsPath());
    //this.posts = this.postsCollection.valueChanges();



    // this.posts.subscribe(data=>{
    //   console.log("data : ", data);
    //   debugger;
    // })

    this.starterQuestions = this.questionsCollection.snapshotChanges().map(actions => {
      return actions.map(action => {
        const data = action.payload.doc.data() as StarterQuestion;
        const id = action.payload.doc.id;

        let q: StarterQuestion = new StarterQuestion();
        q.id = id;
        q.text = data.text;
        console.log(q)
        return q;
      });
    });
  }

  onAnswerQuestionClick(question:StarterQuestion):void{
      // prepare new post with uid
      // copy question id to new post

    let newPost: Post = this.postsService.create();
    newPost.starterQuestionId = question.id;
    newPost.title = question.text;

    //save post draft first, then navigate to edit post

    this.postsService.save(newPost)
      .then(()=>{
        console.log("POST "+ newPost.id+"DRAFT SAVED, going to new post screen...")
        this.postsService.pendingPost = newPost;
        this.router.navigate(['createPost']);
      })
      .catch((error)=>{
        console.log("ERROR SAVING POST DRAFT :");
        console.error(error);
        debugger;
      });
  }

}
