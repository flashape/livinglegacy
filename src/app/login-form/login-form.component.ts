import {Component, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {TrialService} from '../trial.service';
import {NgModel} from '@angular/forms';
import {AuthService} from '../auth.service';

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.css']
})
export class LoginFormComponent implements OnInit {

  constructor(private authService: AuthService, private trialService: TrialService) { }

  email: string;
  password: string;
  passwordVerify: string;
  errorMessage: string;

  @Input()
  showLogin = true;

  @Input()
  showCreateAccount =  false;

  @Input()
  disableLogin =  false;

  showProgress = false;

  @Input()
  headerMessage:string;

  @Output()
  onLoginSuccess:EventEmitter<boolean> = new EventEmitter<boolean>();

  @Output()
  onCreateAccountSuccess:EventEmitter<boolean> = new EventEmitter<boolean>();

  @Output()
  onError:EventEmitter<any> = new EventEmitter<any>();

  showError = false;


  @ViewChild('createEmail') createEmail:NgModel
  @ViewChild('createPass') createPass:NgModel
  @ViewChild('createPass2') createPass2:NgModel

  ngOnInit() {

  }

  setState(name: string){
    switch (name){
      case 'login':
        this.showLogin = true;
        this.showCreateAccount = false;
        break;
      case 'createAccount':
        this.showLogin = false;
        this.showCreateAccount = true;
    }
  }

  login() {
    console.log(`email : ${this.email}, password : ${this.password}`);

    this.showProgress = true;
    this.authService.loginWithEmailAndPassword(this.email, this.password)
      .then(data=>{
        console.log("Login Success!  data : ", data);
        this.onLoginSuccess.emit(true);
      })
      .catch((error) => {
        // Handle Errors here.
        var errorCode = error.code;
        var errorMessage = error.message;
        this.errorMessage = error.message;
        this.onError.emit(error);

      })

  }

  logout(){
    this.authService.logout().then(()=> {
      // Sign-out successful.
      // console.log("Successfully signed out.");
    }).catch((error) => {
      console.error(error);
    });
  }

  onCreateAccountClick(){
    console.log("onCreateAccountClick()");
    console.log("this.creteEmail : ", this.createEmail);
    this.showError = false;

    let email = this.createEmail.value;
    let pass = this.createPass.value;
    this.showProgress = true;
    this.authService.createAccount(email, pass)
      .then(user =>{
        console.log("create user success!!");
        console.log("User : ", user);
        this.authService.createUserDoc(user)
        this.onCreateAccountSuccess.emit(true);
        debugger;
      })

      .catch((error) => {
        // Handle Errors here.
        var errorCode = error.code;
        var errorMessage = error.message;
        this.errorMessage = error.message;
        this.showError = true;
        this.showProgress = false;

        console.error(error)
        this.onError.emit(error);

      })

  }

  // validateForm(email: string, password: string): boolean {
  //   if (email.length === 0) {
  //     this.errorMessage = 'Please enter Email!'
  //     return false
  //   }
  //
  //   if (password.length === 0) {
  //     this.errorMessage = 'Please enter Password!'
  //     return false
  //   }
  //
  //   if (password.length < 6) {
  //     this.errorMessage = 'Password should be at least 6 characters!'
  //     return false
  //   }
  //
  //   this.errorMessage = '';
  //
  //   return true
  // }
  //
  // isValidMailFormat(email: string) {
  //   const EMAIL_REGEXP = /^[a-z0-9!#$%&'*+\/=?^_`{|}~.-]+@[a-z0-9]([a-z0-9-]*[a-z0-9])?(\.[a-z0-9]([a-z0-9-]*[a-z0-9])?)*$/i;
  //
  //   if ((email.length === 0) && (!EMAIL_REGEXP.test(email))) {
  //     return false;
  //   }
  //
  //   return true;
  // }


}
