export class Post {
​
  id: string;
  userId: string = null;
  createdAt: number = null
  updatedAt: number = null;
  title: string = null;
  text: string = "";
  media:Array<any> = null;
  starterQuestionId: string = null;
  comments = null;
  state = "draft";

  toJson():any {
    return {
      id: this.id,
      userId: this.userId,
      createdAt: this.createdAt,
      updatedAt: this.updatedAt,
      title: this.title,
      text: this.text,
      starterQuestionId: this.starterQuestionId,
      state: this.state
    }
  }

  fromJson(json:any):any {
    let post:Post = new Post();

    // return {
    //   id: this.id,
    //   userId: this.userId,
    //   createdAt: this.createdAt,
    //   updatedAt: this.updatedAt,
    //   title: this.title,
    //   text: this.text,
    //   starterQuestionId: this.starterQuestionId,
    //   state: this.state
    // }
  }
}
