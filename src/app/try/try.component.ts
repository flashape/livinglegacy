import {Component, ElementRef, Input, OnInit, ViewChild} from '@angular/core';
import * as localforage from "localforage";
import {SwiperConfigInterface} from "ngx-swiper-wrapper";
import {DomSanitizer, SafeUrl} from '@angular/platform-browser';
import {TrialService} from '../trial.service';
import {TempFile} from '../temp-file';
import {Router} from '@angular/router';
import {AuthService} from '../auth.service';


import * as bowser from "bowser";
import {MediaService} from '../media-service';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {environment} from "../../environments/environment";
import {MediaRecorderComponent} from "../media-recorder/media-recorder.component";
declare var MediaRecorder: any;

@Component({
  selector: 'app-try',
  templateUrl: './try.component.html',
  styleUrls: ['./try.component.css']
})
export class TryComponent implements OnInit {

  private _postText:string = '';
  private isStopped = false;

  get postText(): string {
    return this._postText;
  }

  @Input()
  set postText(value: string) {

    this._postText = value;
    this.trialService.postText = value;
    localforage.setItem('trial_post_text', value)
      .then((data)=>{
        console.log('trial post text stored successfully!');
      })

  }
  @ViewChild('postTextArea') postTextArea:ElementRef;
  @ViewChild('photoInput') photoInput:ElementRef;
  @ViewChild('videoInput') videoInput:ElementRef;
  @ViewChild('audioInput') audioInput:ElementRef;
  @ViewChild('captionTextArea') captionTextArea:ElementRef;
  @ViewChild('audioPlayer') audioPlayer:ElementRef;


  mainContentSwiperConfig: SwiperConfigInterface = {
    direction: 'horizontal',
    keyboard: true,
    centeredSlides: true,
    autoHeight: true
  };

  thumbSwiperConfig: SwiperConfigInterface = {
    direction: 'horizontal',
    slidesPerView: 3,
    spaceBetween: 30,
    keyboard: false,
    centeredSlides: false,
    autoHeight: true,
    pagination: {
      el: '.swiper-pagination',
    }
  };

  tempFiles: TempFile[] = [];

  createAccountSuccessMessages = [];
  createAccountErrorMessages = [];


  private pendingFilesToSave:File[];
  private saveFileIndex = 0;
  private arrayBuffersToStore = [];

  showEditCaption =  false;
  fileForCaptionEdit:TempFile;

  showLogin = false;

  showCreateAccountResultDialog = false;
  showCreateAccountSuccess = false;
  showCreateAccountError = false;

  showMediaRecorder = false;

  //private mediaRecorder:any;
  private audioChunks:Array<any>;

  @ViewChild(MediaRecorderComponent)
  mediaRecorder: MediaRecorderComponent;

  constructor(
    private sanitizer: DomSanitizer,
    private trialService: TrialService,
    private router: Router,
    private authService: AuthService,
    private mediaService: MediaService,
    private http: HttpClient
  ) {
    //localforage.removeItem('trial_images');

  }

  ngOnInit() {
    localforage.getItem('trial_post_text')
      .then(savedText =>{
        if(savedText){
          this.postText = <string>savedText;
        }
      })

    localforage.getItem('trial_images')
      .then(images =>{
        console.log("saved images : ",images)
        if(images){
          let tempFiles = images as Array<TempFile>;
          tempFiles.forEach(file=>{
            let blob:Blob = new Blob([file.arrayBuffer])
            file.url = this.sanitizer.bypassSecurityTrustUrl( window.URL.createObjectURL(blob) );
            file.blob = blob;
          })
          this.tempFiles = tempFiles;
          this.trialService.selectedFiles = this.tempFiles;
        }
      })

   // this.createAccountSuccessMessages.push({severity:'success', summary:'Account Created'});

  }




  onLaunchRecordingClick(){
    //this.recordVideo();
    this.showMediaRecorder = true;

  }

  addDocument(){
    this.photoInput.nativeElement.click();
  }

  addPicture(){
    console.log("Attempting to open file select...")
    this.photoInput.nativeElement.click();
  }



  // recordVideo(){
  //   console.log("recordVideo()")
  //   // this.mediaService.getInputDevices()
  //   //   .then(devices=>{
  //   //     console.log("GOT INPUT DEVICES : ", devices)
  //   //   })
  //   //   .catch(error =>{
  //   //     console.error(error);
  //   //   })
  //
  //
  //   if(bowser.mobile || bowser.tablet){
  //     // on mobile just use the file inputs to record audio/video
  //     this.videoInput.nativeElement.click();
  //
  //   }else if(bowser.check({chrome: "49", firefox:'25'})){
  //     // use the MediaRecorder API
  //     navigator.mediaDevices.getUserMedia({ audio: true, video: false })
  //       .then(stream =>{
  //
  //         this.audioChunks = [];
  //         console.log("stream : ", stream)
  //         this.audioPlayer.nativeElement.srcObject = stream;
  //
  //         this.audioPlayer.nativeElement.onpause = () => {
  //           alert("The video has been paused");
  //           this.mediaRecorder.pause();
  //         };
  //
  //         this.audioPlayer.nativeElement.onplay = () => {
  //           alert("The video has been paused");
  //           this.mediaRecorder.resume();
  //         };
  //
  //         let options = {};
  //         this.mediaRecorder = new MediaRecorder(stream, options);
  //         this.mediaRecorder.ignoreMutedMedia = true;
  //         this.mediaRecorder.ondataavailable = (event)=>{
  //           console.log("ondataavailable : ", event)
  //           this.audioChunks.push(event.data);
  //
  //           if(this.isStopped){
  //             this.replayAudio();
  //           }
  //         }
  //
  //         this.mediaRecorder.start();
  //         this.audioPlayer.nativeElement.play();
  //
  //         // if (window.URL) {
  //         //   this.audioPlayer.nativeElement.src = window.URL.createObjectURL(stream);
  //        // // } else {
  //         //   this.audioPlayer.nativeElement.src = stream;
  //         // }
  //       });
  //   }else{
  //     // alert user their browser is not supported
  //   }
  // }

  replayAudio(){
    console.log("replayAudio()")
    let blob = new Blob(this.audioChunks, { 'type' : 'audio/ogg; codecs=opus' });
    //this.audioChunks = [];
    let audioURL = window.URL.createObjectURL(blob);
    this.audioPlayer.nativeElement.srcObject = null;
    this.audioPlayer.nativeElement.src = audioURL;
    this.audioPlayer.nativeElement.load();



    //this.audioPlayer.nativeElement.srcObject = this.mediaRecorder.stream;
    this.audioPlayer.nativeElement.play();
  }

  stopRecordingAudio() {
    // this.isStopped = true;
    // this.audioPlayer.nativeElement.pause();
    // this.mediaRecorder.stop();
    //
    // this.audioPlayer.nativeElement.onpause = null;
    // this.audioPlayer.nativeElement.onplay = null;

  }

  onPhotoInputChange(event){
    let file = event.target.files[0];

    if(!file){
      return;
    }
    this.pendingFilesToSave = event.target.files;
    this.startStoringFiles();


  }


  private startStoringFiles(){
    console.log("startStoringFiles(), files : ", this.pendingFilesToSave)
    this.saveFileIndex = 0;
    this.readFile(this.pendingFilesToSave[this.saveFileIndex]);
  }

  nextFileUpload(){
    console.log("this.saveFileIndex : ", this.saveFileIndex);
    console.log("this.pendingFilesToSave : ", this.pendingFilesToSave)
    console.log("this.pendingFilesToSave.length : ", this.pendingFilesToSave.length);

    if(this.saveFileIndex < this.pendingFilesToSave.length-1){
      this.saveFileIndex++;
      this.readFile(this.pendingFilesToSave[this.saveFileIndex]);
    }else{
      this.onReadFilesFinished();
    }
  }

  onReadFilesFinished(){
    this.photoInput.nativeElement.value = '';
    this.storeTempFiles();
  }

  readFile(file){
    let reader: FileReader = new FileReader();

    reader.readAsArrayBuffer(file);

    reader.onloadend = (event)=>{
      let result = reader.result;
      let buffer: ArrayBuffer = result;

      let tempFile: TempFile = new TempFile();

      // TODO: copy any required File object properties to tempFile
      tempFile.url = this.sanitizer.bypassSecurityTrustUrl( window.URL.createObjectURL(file) );;
      tempFile.type = 'image';
      tempFile.mimeType = file.type;
      tempFile.name = file.name;

      tempFile.arrayBuffer = buffer;

      tempFile.file = file;

      console.log('tempFile : ', tempFile);

      this.tempFiles.push( tempFile );
      this.trialService.selectedFiles.push(tempFile);
      this.nextFileUpload();
    }

  }

  storeTempFiles(){

    localforage.setItem('trial_images', this.tempFiles)
      .then((data)=>{
        console.log('Temp files stored successfully!');
      })
  }



  onVideoInputChange(event){
    // let file = event.target.files[0];
    //
    // if(!file){
    //   return;
    // }
    //
    // this.pendingFilesToSave = event.target.files;
    //
    // this.startStoringFiles();

  }

  onAudioInputChange(event){
    // let file = event.target.files[0];
    //
    // if(!file){
    //   return;
    // }
    //
    // this.pendingFilesToSave = event.target.files;
    //
    // this.startStoringFiles();

  }

  editCaption(file:TempFile){
    console.log('temp fiel clicked:', file);
    this.fileForCaptionEdit = file;

    this.captionTextArea.nativeElement.value = file.caption || "";
    this.showEditCaption = true;
  }

  onCaptionCancel(){
    this.showEditCaption = false;
    this.captionTextArea.nativeElement.value = "";

  }

  onCaptionSave(){
    this.showEditCaption = false;
    this.fileForCaptionEdit.caption = this.captionTextArea.nativeElement.value;
    this.storeTempFiles();
    this.captionTextArea.nativeElement.value = "";
  }

  onSaveClick(){
    console.log("save clicked!");

    if(this.authService.authenticated){

    }else{
      this.showLogin = true;
    }

  }

  onSaveMouseDown(event){
    event.preventDefault();
  }

  onCreateAccountSuccess(){
    this.showLogin = false;
    this.createAccountSuccessMessages.push({severity: 'success', summary: 'Account Created'});

    this.showCreateAccountResultDialog = true;
    this.showCreateAccountSuccess = true;
  }


  selectPlan(){
    this.trialService.hasLocalSavedData = (this.postText.length > 0 || this.tempFiles.length > 0);
    this.router.navigate(['pricing']);
  }
  onCreateAccountClick(){
    // this.router.navigate(['login']);


  }

  onCreateAccountCancel(){
    //this.showCreateAccountPopup = false;
  }
}
