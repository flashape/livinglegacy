import { TestBed, inject } from '@angular/core/testing';

import { OpenTokService } from './open-tok.service';

describe('OpenTokService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [OpenTokService]
    });
  });

  it('should be created', inject([OpenTokService], (service: OpenTokService) => {
    expect(service).toBeTruthy();
  }));
});
