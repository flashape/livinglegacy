import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from "./home/home.component";
import { VaultComponent } from "./vault/vault.component";
import { PricingComponent } from "./pricing/pricing.component";
import { UserComponent } from "./user/user.component";
import { GetStartedComponent } from "./get-started/get-started.component";
import { AuthGuard } from './auth.guard';
import {LoginComponent} from './login/login.component';
import {EditPostComponent} from './edit-post/edit-post.component';
import {ViewPostComponent} from './view-post/view-post.component';
import {AboutComponent} from "./about/about.component";
import {MemoriesOfMeComponent} from './memories-of-me/memories-of-me.component';
import {PostResolver} from './post-resolver';
import {TryComponent} from './try/try.component';
import {TrialSignupComponent} from './trial-signup/trial-signup.component';
import {MyAccountComponent} from './my-account/my-account.component';

const appRoutes: Routes = [
    { path: 'home', component: HomeComponent },
    { path: 'about', component: AboutComponent },
    { path: 'vault', component: VaultComponent, canActivate: [AuthGuard]},
    { path: 'questions', component: MemoriesOfMeComponent},
    { path: 'user/:userId/post/:postId',
      component: ViewPostComponent,
      resolve: {
        post: PostResolver
      }
    },
    { path: 'user', component: UserComponent },
    { path: 'my-account', component: MyAccountComponent },
    { path: 'pricing', component: PricingComponent },
    { path: 'try', component: TryComponent },
    { path: 'trial-signup', component: TrialSignupComponent },
    { path: 'get-started', component: GetStartedComponent },
    { path: 'login', component: LoginComponent },
    { path: 'createPost', component: EditPostComponent},
    { path: 'editPost/:postId',
      component: EditPostComponent,
      resolve: {
        post: PostResolver
      }
    },
    { path: '', redirectTo: 'home', pathMatch: 'full' },
    { path: '**', redirectTo: '/not-found' }

];

@NgModule({
    imports: [
        RouterModule.forRoot(appRoutes)
    ],
    exports: [RouterModule]
})

export class AppRouteModule {

}
