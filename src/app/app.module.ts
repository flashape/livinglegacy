import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';

import { NgModule } from '@angular/core';
import { FlexLayoutModule } from '@angular/flex-layout';
//import { StickyModule } from 'ng2-sticky-kit';
import { AngularFireModule } from 'angularfire2';
import { AngularFirestoreModule } from 'angularfire2/firestore';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';



import { ButtonModule } from 'primeng/components/button/button';
import { PasswordModule } from 'primeng/components/password/password';
import { MessageModule } from 'primeng/components/message/message';
import { MessagesModule } from 'primeng/components/messages/messages';
import {InputTextareaModule} from 'primeng/components/inputtextarea/inputtextarea';
import {CarouselModule} from 'primeng/components/carousel/carousel';
import {CardModule} from 'primeng/components/card/card';
import {LightboxModule} from 'primeng/components/lightbox/lightbox';
import {ConfirmDialogModule} from 'primeng/components/confirmdialog/confirmdialog';
import {ConfirmationService} from 'primeng/components/common/api';
import {DialogModule} from 'primeng/components/dialog/dialog';
import {ProgressBarModule} from 'primeng/components/progressbar/progressbar';

import { ArchwizardModule } from 'ng2-archwizard/dist';


import { SwiperModule } from 'ngx-swiper-wrapper';
import { SWIPER_CONFIG } from 'ngx-swiper-wrapper';
import { SwiperConfigInterface } from 'ngx-swiper-wrapper';

import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { VaultComponent } from './vault/vault.component';
import { UserComponent } from './user/user.component';
import { PricingComponent } from './pricing/pricing.component';
import { GetStartedComponent } from './get-started/get-started.component';
import { AppRouteModule } from "./app.route";
import { HeaderComponent } from './header/header.component';
import { AuthService } from './auth.service';
import {AuthGuard} from './auth.guard';
import {AngularFireAuth} from 'angularfire2/auth';
import { LoginComponent } from './login/login.component';
import { EditPostComponent } from './edit-post/edit-post.component';
import {PostsService} from './posts.service';
import { ViewPostComponent } from './view-post/view-post.component';
import { AboutComponent } from './about/about.component';
import { MemoriesOfMeComponent } from './memories-of-me/memories-of-me.component';
import {PostResolver} from './post-resolver';
import { TryComponent } from './try/try.component';
import {TrialService} from './trial.service';
import { TrialSignupComponent } from './trial-signup/trial-signup.component';
import { CreateAccountComponent } from './create-account/create-account.component';
import {HttpClient} from '@angular/common/http';
import { LoginFormComponent } from './login-form/login-form.component';
import { MyAccountComponent } from './my-account/my-account.component';
import {MediaService} from './media-service';
import { MediaRecorderComponent } from './media-recorder/media-recorder.component';
import {OpenTokService} from './open-tok.service';

// Initialize Firebase
// var firebaseConfig = {
//   apiKey: "AIzaSyADyulGCF938oM1SFXcWj3tmUJnG83g4Kk",
//   authDomain: "living-legacy.firebaseapp.com",
//   databaseURL: "https://living-legacy.firebaseio.com",
//   projectId: "living-legacy",
//   storageBucket: "living-legacy.appspot.com",
//   messagingSenderId: "519240742447"
// };


 var firebaseConfig = {
   apiKey: "AIzaSyDsYe0Zelt0Ky48-r9tiKlaEYAzf7nYa8k",
   authDomain: "living-legacy-ab939.firebaseapp.com",
   databaseURL: "https://living-legacy-ab939.firebaseio.com",
   projectId: "living-legacy-ab939",
   storageBucket: "living-legacy-ab939.appspot.com",
   messagingSenderId: "104016004461"
 };

declare var firebase : any;
//firebase.initializeApp(firebaseConfig);


const DEFAULT_SWIPER_CONFIG: SwiperConfigInterface = {
  direction: 'horizontal',
  slidesPerView: 'auto',
  keyboard: true
};

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    VaultComponent,
    UserComponent,
    PricingComponent,
    GetStartedComponent,
    HeaderComponent,
    LoginComponent,
    EditPostComponent,
    ViewPostComponent,
    AboutComponent,
    MemoriesOfMeComponent,
    TryComponent,
    TrialSignupComponent,
    CreateAccountComponent,
    LoginFormComponent,
    MyAccountComponent,
    MediaRecorderComponent
  ],
  imports: [
    AngularFireModule.initializeApp(firebaseConfig),
    AngularFirestoreModule,
    HttpClientModule,
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    AppRouteModule,
    FlexLayoutModule,
    ButtonModule,
    MessageModule,
    MessagesModule,
    PasswordModule,
    InputTextareaModule,
    CarouselModule,
    CardModule,
    LightboxModule,
    ConfirmDialogModule,
    SwiperModule,
    DialogModule,
    ProgressBarModule

  ],
  providers: [
    {
    provide: SWIPER_CONFIG,
    useValue: DEFAULT_SWIPER_CONFIG
    },
    AuthService,
    AuthGuard,
    AngularFireAuth,
    PostsService,
    ConfirmationService,
    PostResolver,
    TrialService,
    HttpClient,
    MediaService,
    OpenTokService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
