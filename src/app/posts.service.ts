import { Injectable } from '@angular/core';
import {Post} from './model/post';
import {AngularFirestore, AngularFirestoreDocument} from 'angularfire2/firestore';
import {AuthService} from './auth.service';
import {Observable} from 'rxjs/Observable';

@Injectable()
export class PostsService {

  pendingPost: Post;

  constructor(private afs: AngularFirestore, private authService: AuthService) { }

  addNew(post:Post){
    // this.postsCollection.doc(postID).set(this.pendingPost.toJson())
    //   .then(()=>{
    //     console.log("add post success!!")
    //     debugger;
    //   })
    //   .catch(e=>{
    //     console.log("ERROR ADDNG POST!")
    //     debugger;
    //   })

  }

  create():Post {

    let newPost: Post = new Post();

    newPost.id = this.afs.createId();
    newPost.userId = this.authService.userId;
    return newPost;
  }

  getPost(id: string): Observable<Post> {
    console.log("PostsService.getPost()")
    let postDoc: AngularFirestoreDocument<Post> = this.afs.doc<Post>(this.getUserPostsPath() + '/' + id);
    let item = postDoc.valueChanges();
    console.log("item : ", item)
    return item;
  }

  getUserPostsPath(): string{
    return 'users/'+this.authService.userId+'/posts'
  }

  getMediaItemsPath(post:Post): string{
    return this.getPostPath(post.id+"/mediaItems");
  }


  getPostPath(id: string): string{
    return this.getUserPostsPath() + '/' + id;
  }

  save (post:Post): Promise<any>{
    if (typeof post.toJson === "function") {
      post = post.toJson();
    }

    console.log("saving to post path : ", this.getUserPostsPath())
    console.log("post id : ", post.id)
    return this.afs.collection( this.getUserPostsPath() ).doc(post.id).set(post);
  }

  updateStarterQuestionsAnswered(postId, starterQuestionId):Promise<any> {
    let key = `starterQuestionsAnswered.${starterQuestionId}`;
    let data = {};
    data[key] = postId;

    return this.afs.doc('users/'+this.authService.userId).update(data);
  }

  addMediaItemToPost(postId, snapshot, caption = null):Promise<any> {
    let fileData:any = {};

    fileData.file = JSON.parse(JSON.stringify(snapshot.metadata));
    fileData.caption = caption;

    let postPath = this.getPostPath(postId);

    console.log("adding media item to postPath : ", postPath);

    return this.afs.doc(postPath).collection("/mediaItems").add(fileData);

  }

  getUserStoragePath() {
    return 'users/'+this.authService.userId;
  }
}
