import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {environment} from "../../environments/environment";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {OpenTokService} from '../open-tok.service';
import * as OT from '@opentok/client';

@Component({
  selector: 'app-media-recorder',
  templateUrl: './media-recorder.component.html',
  styleUrls: ['./media-recorder.component.css']
})
export class MediaRecorderComponent implements OnInit {

  constructor(private openTok: OpenTokService) { }

  initializing = false;

  @ViewChild('publisherDiv') publisherDiv: ElementRef;


  publisher: OT.Publisher;
  publishing: Boolean;

  session: OT.Session;

  ngOnInit() {
    this.openTok.initSession()
      .subscribe(
        (session)=>{
          console.log("success creating opentok session : ", session);

        },
        (error)=>{
          console.log("ERROR creating opentok session : ", error);
        }
      )
  }

  recordVideo(){
    console.log("Record video clicked!")
    this.createPublisher();

    if (this.session) {
      if (this.session['isConnected']()) {
        this.publish();
      }
      this.session.on('sessionConnected', () => this.publish());
    }
  }

  recordAudio(){
    console.log("Record audio clicked!")

    // var pubOptions = {publishAudio:true, publishVideo:false};
    //
    // // Replace replacementElementId with the ID of the DOM element to replace:
    // publisher = OT.initPublisher(replacementElementId, pubOptions);
  }
  recordFromMobile(){
    console.log("Record from mobile clicked!")
  }

  createPublisher(){
    this.publisher = OT.initPublisher(this.publisherDiv.nativeElement, {insertMode: 'append'});


    // publisher = OT.initPublisher('publisher', {}, function (err) {
    //   if (err) {
    //     if (err.name === 'OT_USER_MEDIA_ACCESS_DENIED') {
    //       // Access denied can also be handled by the accessDenied event
    //       showMessage('Please allow access to the Camera and Microphone and try publishing again.');
    //     } else {
    //       showMessage('Failed to get access to your camera or microphone. Please check that your webcam'
    //         + ' is connected and not being used by another application and try again.');
    //     }
    //     publisher.destroy();
    //     publisher = null;
    //   }
    // });

  }

  publish() {
    this.session.publish(this.publisher, (err) => {
      if (err) {
        alert(err.message);
      } else {
        this.publishing = true;
      }
    });


    // session.publish(publisher, function(err) {
    //   if (err) {
    //     switch (err.name) {
    //       case "OT_NOT_CONNECTED":
    //         showMessage("Publishing your video failed. You are not connected to the internet.");
    //         break;
    //       case "OT_CREATE_PEER_CONNECTION_FAILED":
    //         showMessage("Publishing your video failed. This could be due to a restrictive firewall.");
    //         break;
    //       default:
    //         showMessage("An unknown error occurred while trying to publish your video. Please try again later.");
    //     }
    //     publisher.destroy();
    //     publisher = null;
    //   }
    // });
  }




}
