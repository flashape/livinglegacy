import { Injectable } from '@angular/core';
import {AngularFireAuth} from 'angularfire2/auth';
import {AngularFirestore} from 'angularfire2/firestore';
import {Router} from '@angular/router';
import {User} from "firebase";

@Injectable()
export class AuthService {

  user: any = null;
  userDoc: any;
  userInfo: any;

  fui_auth: any;

  constructor(
    public afAuth: AngularFireAuth,
    private afs: AngularFirestore,
    private router: Router) {

    this.afAuth.authState.subscribe( user =>{

      this.user = user;

      if (user) {
        console.log('User logged in : ', user);
        this.userDoc = this.afs.doc("users/"+this.user.uid);
        this.userDoc.valueChanges().subscribe((userInfo)=>{
          this.userInfo = userInfo;
          console.log("this.userInfo : ", this.userInfo);

        })

        console.log("this.userDoc : ", this.userDoc);

        //this.router.navigate(['/questions']);
      }else{
        console.log('User signed out.');
        //this.router.navigate(['/']);
      }

    })

  }

  get userId(){
    return this.user.uid;
  }

  // Returns true if user is logged in
  get authenticated(): boolean {
    console.log('AuthService.authenticated user : ', this.user)
    return this.user !== null;
  }


  loginWithEmailAndPassword(email: string, password: string): Promise<User> {
    return this.afAuth.auth.signInWithEmailAndPassword(email, password);
  }


  createAccount(email: string, password: string): Promise<User> {
    return this.afAuth.auth.createUserWithEmailAndPassword(email, password);

  }

  createUserDoc(user){
    this.afs.doc('users/'+user.uid).set({
      firstName: "",
      lastName: "",
      email: user.email
    })
      .then(data=>{
        console.log("SUCCESSFULLY SAVED USER DOC..")
        console.log(data);
      })
      .catch(error=>{
      console.log("error creating user doc: ")
      console.log(error);
    })
  }


  logout(): Promise<void> {
    return this.afAuth.auth.signOut();
  }
}
