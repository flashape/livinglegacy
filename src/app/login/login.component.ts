import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {AuthService} from "../auth.service";

import * as firebase from 'firebase/app'
import {Message, UIMessage} from "primeng/primeng";
import {TrialService} from '../trial.service';
import {NgModel} from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})



export class LoginComponent implements OnInit {


  constructor(private authService: AuthService, private trialService: TrialService) { }

  email: string;
  password: string;
  passwordVerify: string;
  errorMessage: string;

  showLogin = true;t
  showCreateAccount =  false;


  @ViewChild('createEmail') createEmail:NgModel
  @ViewChild('createPass') createPass:NgModel
  @ViewChild('createPass2') createPass2:NgModel

  ngOnInit() {

  }

  setState(name: string){
    switch (name){
      case 'login':
        this.showLogin = true;
        this.showCreateAccount = false;
        break;
      case 'createAccount':
        this.showLogin = false;
        this.showCreateAccount = true;
    }
  }

  login() {
    console.log(`email : ${this.email}, password : ${this.password}`);


    this.authService.loginWithEmailAndPassword(this.email, this.password).catch((error) => {
      // Handle Errors here.
      var errorCode = error.code;
      var errorMessage = error.message;
      this.errorMessage = error.message;
    });

  }

  logout(){
    this.authService.logout().then(()=> {
      // Sign-out successful.
      // console.log("Successfully signed out.");
    }).catch((error) => {
      console.error(error);
    });
  }

  onCreateAccountClick(){
    console.log("onCreateAccountClick()");
    console.log("this.creteEmail : ", this.createEmail)
    let email = this.createEmail.value;
    let pass = this.createPass.value;

    this.authService.createAccount(email, pass)
      .catch((error) => {
      // Handle Errors here.
      var errorCode = error.code;
      var errorMessage = error.message;
      this.errorMessage = error.message;
    })
      .then(user =>{
        console.log("create user success!!");
        console.log("User : ", user);
        debugger;
      })
  }

  // validateForm(email: string, password: string): boolean {
  //   if (email.length === 0) {
  //     this.errorMessage = 'Please enter Email!'
  //     return false
  //   }
  //
  //   if (password.length === 0) {
  //     this.errorMessage = 'Please enter Password!'
  //     return false
  //   }
  //
  //   if (password.length < 6) {
  //     this.errorMessage = 'Password should be at least 6 characters!'
  //     return false
  //   }
  //
  //   this.errorMessage = '';
  //
  //   return true
  // }
  //
  // isValidMailFormat(email: string) {
  //   const EMAIL_REGEXP = /^[a-z0-9!#$%&'*+\/=?^_`{|}~.-]+@[a-z0-9]([a-z0-9-]*[a-z0-9])?(\.[a-z0-9]([a-z0-9-]*[a-z0-9])?)*$/i;
  //
  //   if ((email.length === 0) && (!EMAIL_REGEXP.test(email))) {
  //     return false;
  //   }
  //
  //   return true;
  // }



}
