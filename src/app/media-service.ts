import { Injectable } from '@angular/core';

@Injectable()
export class MediaService {

  devices: Array<MediaDeviceInfo>;

  constructor() { }

  // filter:  "audioinput" or "videoinput"
  getInputDevices(filter): Promise<Array<MediaDeviceInfo>>{
    console.log("getInputDevices()")
    return navigator.mediaDevices.enumerateDevices()
      .then(function(devices) {
        console.log("got devices : ", devices)
        if(!filter){
          return Promise.resolve(devices)
        }else{

          let filtered = [];
          devices.forEach(function(device) {

            let menu = document.getElementById("inputdevices");
            if (device.kind === filter) {
              filtered.push(device);
            }
          })
          console.log("retruning filtered: ", filtered)
          return Promise.resolve(filtered);
        }

      })
      .catch(function(err) {
        console.log(err.name + ": " + err.message);
        Promise.reject(err);
      });
  }

}
