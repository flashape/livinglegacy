import {Component, ElementRef, Inject, OnInit, ViewChild} from '@angular/core';
import {PostsService} from '../posts.service';
import {AuthService} from '../auth.service';
import {Post} from '../model/post';
import {AngularFireAuth} from 'angularfire2/auth';
import {AngularFirestore, AngularFirestoreCollection} from 'angularfire2/firestore';
import { FirebaseApp } from 'angularfire2';

// declare var firebase : any;
import * as firebase from 'firebase/app'
import 'firebase/storage';
import {StarterQuestion} from '../model/starter-question';
import {Observable} from 'rxjs/Observable';
import {ActivatedRoute, ActivatedRouteSnapshot, Router} from '@angular/router';
import {ConfirmationService} from 'primeng/primeng';

@Component({
  selector: 'app-edit-post',
  templateUrl: './edit-post.component.html',
  styleUrls: ['./edit-post.component.css']
})
export class EditPostComponent implements OnInit {

  private _post:Post;

  get post(): Post {
    return this._post;
  }

  set post(value: Post) {
    this._post = value;
  }

  photosCollection: AngularFirestoreCollection<any>;
  photos: Observable<any[]>;

  showSuccessMessage = false;


  @ViewChild('postText') postText:ElementRef;
  @ViewChild('photoInput') photoInput:ElementRef;
  @ViewChild('videoInput') videoInput:ElementRef;

  private uploadIndex = 0;
  private filesToUpload;

  constructor(
    private elementRef: ElementRef,
    public postsService: PostsService,
    private authService: AuthService,
    private firebaseApp: FirebaseApp,
    private afs:AngularFirestore,
    private router:Router,
    private confirmationService:ConfirmationService,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    let snapshot:ActivatedRouteSnapshot = this.route.snapshot;
    console.log("snapshot : ", snapshot);

    this.route.data
      .subscribe((data) => {
        console.log(data)
        this.post = data.post;
      });


    if(snapshot.url[0].path == "createPost"){
      console.log("USING PENDING POST...")
      this.post = this.postsService.pendingPost;

    }
    this.photosCollection = this.afs.collection(this.postsService.getMediaItemsPath(this._post));

    this.photos = this.photosCollection.snapshotChanges().map(actions => {
      return actions.map(action => {
        const data = action.payload.doc.data();
        const id = action.payload.doc.id;
        //
        // let q: StarterQuestion = new StarterQuestion();
        data.id = id;
        data.editingCaption = false;
        // q.text = data.text;
        console.log("photo : ", data);

        return data;
      });
    });
  }

  addPicture(){
    console.log("Attempting to open file select...")
    this.photoInput.nativeElement.click();
  }

  addVideo(){

  }

  recordAudio(){

  }

  recordVideo(){
    this.videoInput.nativeElement.click();

  }

  onPhotoInputChange(event){
    let file = event.target.files[0];

    if(!file){
      return;
    }

    this.filesToUpload = event.target.files;

    this.startFileUploads();

  }

  private startFileUploads(){
    console.log("startFileUploads(), files : ", this.filesToUpload)
    this.uploadIndex = 0;
    debugger;
    this.uploadFile(this.filesToUpload[this.uploadIndex]);
  }

  nextFileUpload(){
    console.log("this.uploadIndex : ", this.uploadIndex);
    console.log("this.filesToUpload : ", this.filesToUpload)
    console.log("this.filesToUpload.length : ", this.filesToUpload.length);

    if(this.uploadIndex < this.filesToUpload.length-1){
      this.uploadIndex++;
      this.uploadFile(this.filesToUpload[this.uploadIndex]);
    }else{
      this.onUploadsFinished();
    }
  }

  onUploadsFinished(){
    this.photoInput.nativeElement.value = '';
  }

  uploadFile(file){
    var fbApp: any =  this.firebaseApp;
    var fb = fbApp.firebase_;
    let storage = fb.storage();
    let storageRef = storage.ref();


    let uploadTask = storageRef.child( this.postsService.getUserStoragePath()+'/'+file.name+"."+Date.now()).put(file);

    uploadTask.on('state_changed', (snapshot)=>{
        let progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
        console.log('Upload is ' + progress + '% done');

      },
      //error handler
      (error)=>{
        console.log("error putting file : ")
        console.log(error);
        debugger;
      },

      // complete handler...
      () =>{
        console.log("snapshot : ", uploadTask.snapshot);
        console.log('Uploaded file!');

        let postPath = this.postsService.getPostPath(this._post.id);

        return this.postsService.addMediaItemToPost(this._post.id, uploadTask.snapshot)
          .then(()=>{
            console.log("Media item added to post!");
            this.nextFileUpload();

          })
          .catch((e)=>{
            console.log("error putting file : ")
            console.log(e);
            debugger;
          });
      });
  }

  save(){

    this._post.text = this.postText.nativeElement.value;

    if(this._post.state == 'draft'){
      this._post.state = 'published';
    }

    this.postsService.save(this._post)
      .then(()=>{
        console.log("add post success!!")
        return this.postsService.updateStarterQuestionsAnswered(this._post.id, this._post.starterQuestionId)
      })
      .then(()=>{
        console.log("startQuestionsAnswere updated successfully!!")
        debugger;
      })
      .catch(e=>{
        console.log("ERROR ADDNG POST!")
        debugger;
      });
  }

  editCaption(fileData){
    console.log("Editing caption for photo : ", fileData);
    fileData.editingCaption = true;
  }

  saveCaption(fileData){
    console.log("Saving caption for photo : ", fileData);
    let textarea = this.elementRef.nativeElement.querySelector(`#editCaption-${fileData.id} textarea`);
    console.log("textarea : ", textarea)
    let text = textarea.value;
    console.log("text : ", text);
    debugger;

    var fileDocRef = this.afs.collection(this.postsService.getMediaItemsPath(this._post)).doc(fileData.id);

    var setWithMerge = fileDocRef.set({
      caption: text
    }, { merge: true })
      .then(()=>{
        fileData.editingCaption = false;
      })


  }


  deleteThumb(fileData){
    console.log("deleteThumb();")
    var fileDocRef = this.afs.collection(this.postsService.getMediaItemsPath(this._post)).doc(fileData.id);
    this.confirmationService.confirm({
      message: 'Do you want to delete this media item?',
      header: 'Delete Confirmation',
      icon: 'fa fa-trash',
      accept: () => {
        fileDocRef.delete()
          .then(()=>{
            debugger;
          })
      },
      reject: () => {
        //this.msgs = [{severity:'info', summary:'Rejected', detail:'You have rejected'}];
      }
    });



  }

  viewPost($event){
    $event.preventDefault();
    let userId = this._post.userId;
    let postId = this._post.id;

    // let path = `user/${userId}/post/${postId}`;
    // this.router.navigate([path]);

    let path = `user/${userId}/post/${postId}`;
    this.router.navigate(['user', userId, 'post', postId]);
    return false;

  }

}
