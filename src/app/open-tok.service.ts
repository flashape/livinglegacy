import { Injectable } from '@angular/core';
import * as OT from '@opentok/client';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {environment} from '../environments/environment';


@Injectable()
export class OpenTokService {

  session: OT.Session;
  token: string;

  constructor(private http: HttpClient) { }


  getJsonHeaderOption(): any{
    const httpOptions = {};

    let headers = new HttpHeaders({
      'Content-Type': 'application/json'
    });

    httpOptions['headers'] = headers;

    return httpOptions;
  }

  initSession(){
    let httpOptions = this.getJsonHeaderOption();

    let endpoint = environment.apiRoot + 'recordings/session';

    // TODO:  Secure this
    return this.http.post(endpoint, {}, httpOptions )
      .map((result: any) => {
        console.log("result : ", result)
        this.session = OT.initSession(result.apiKey, result.sessionId);
        this.token = result.token;
        return this.session;
      });

  }



}
