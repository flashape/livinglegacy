import {EventEmitter, Injectable} from '@angular/core';
import {TempFile} from './temp-file';
import {Post} from './model/post';
import {Router} from '@angular/router';
import {PostsService} from './posts.service';
import {FirebaseApp} from 'angularfire2';
import * as localforage from "localforage";

@Injectable()
export class TrialService {

  hasLocalSavedData = false;
  selectedFiles: TempFile[] = [];
  postText: string = "";
  newPost: Post;

  private uploadIndex = 0;

  saveComplete:EventEmitter<Post> = new EventEmitter<Post>();

  // TODO:  Choose random starter question
  starterQuestionId = 'H72t46USurbs4ExWszR8';
  questionText = 'How did your family celebrate holidays when you were a child?';

  constructor(private router:Router,
              private postsService:PostsService,
              private firebaseApp: FirebaseApp,
  ) { }


  discard() {
    localforage.removeItem('trial_post_text');
    localforage.removeItem('trial_images');
    this.hasLocalSavedData = false;
    this.postText = null;
    this.selectedFiles = [];
  }

  saveTrialData(){


    // prepare new post with uid
    // copy question id to new post

    this.newPost = this.postsService.create();
    this.newPost.starterQuestionId = this.starterQuestionId;
    this.newPost.title = this.questionText;
    this.newPost.text = this.postText;
    this.newPost.state = 'published';


    this.postsService.save(this.newPost)
      .then(()=>{
        console.log(("psot saved succssfully, updating started questions answered...."))
        return this.postsService.updateStarterQuestionsAnswered(this.newPost.id, this.newPost.starterQuestionId)

      })

      .then(()=>{
        this.startFileUploads();
      })


      .catch((error)=>{
        console.log("ERROR SAVING POST DRAFT :");
        console.error(error);
        debugger;
      });


  }

  private startFileUploads(){
    console.log("startFileUploads(), files : ", this.selectedFiles)
    this.uploadIndex = 0;
    this.uploadFile(this.selectedFiles[this.uploadIndex]);
  }

  nextFileUpload(){
    console.log("this.uploadIndex : ", this.uploadIndex);
    console.log("this.selectedFiles : ", this.selectedFiles)
    console.log("this.selectedFiles.length : ", this.selectedFiles.length);

    if(this.uploadIndex < this.selectedFiles.length-1){
      this.uploadIndex++;
      this.uploadFile(this.selectedFiles[this.uploadIndex]);
    }else{
      this.onUploadsFinished();
    }
  }

  onUploadsFinished(){
    this.discard();
    this.saveComplete.emit(this.newPost);
  }



  uploadFile(tempFile: TempFile){
    var fbApp: any =  this.firebaseApp;
    var fb = fbApp.firebase_;
    let storage = fb.storage();
    let storageRef = storage.ref();

    let filename:string;

    let thingToUpload;
    let metadata;

    if(tempFile.file){
      filename = tempFile.file.name;
      thingToUpload = tempFile.file;
    }else{
      filename = 'blob';
      thingToUpload = tempFile.blob;
      metadata = {
        contentType: tempFile.mimeType,
      };
    }


    console.log("thing to uplaod : ", thingToUpload)
    console.log("metadata " , metadata)
    //let file = tempFile.file;

    let uploadTask = storageRef.child( this.postsService.getUserStoragePath()+'/'+filename+"."+Date.now()).put(thingToUpload, metadata);

    uploadTask.on('state_changed', (snapshot)=>{
        let progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
        console.log('Upload is ' + progress + '% done');

      },
      //error handler
      (error)=>{
        console.log("error putting file : ")
        console.log(error);
        debugger;
      },

      // complete handler...
      () =>{
        console.log("snapshot : ", uploadTask.snapshot);
        console.log('Uploaded file!');

        let postPath = this.postsService.getPostPath(this.newPost.id);

        return this.postsService.addMediaItemToPost(this.newPost.id, uploadTask.snapshot, tempFile.caption)
          .then(()=>{
            console.log("Media item added to post!");
            this.nextFileUpload();

          })
          .catch((e)=>{
            console.log("error putting file : ")
            console.log(e);
            debugger;
          });
      });
  }
}
