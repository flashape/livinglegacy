"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const functions = require("firebase-functions");
const admin = require("firebase-admin");
const Stripe = require("stripe");
// // Start writing Firebase Functions
// // https://firebase.google.com/functions/write-firebase-functions
//
// export const helloWorld = functions.https.onRequest((request, response) => {
//  response.send("Hello from Firebase!");
// });
const cors = require("cors");
const express = require("express");
const OpenTok = require("opentok");
// there is an issue with typescript types for stripe,
// see here: https://github.com/stripe/stripe-node/issues/296#issuecomment-335308245
// // Then do whatever you like:
// const customer: Promise<StripeNode.customers.ICustomer> = stripe.customers.create( ... )
const stripeKey = 'sk_test_9f5V4lK0UFjb8X3ZAPSiJ1wX';
const stripe = new Stripe(stripeKey);
const OPENTOK_API_KEY = '46072372';
const OPENTOK_SECRET = '0c504a98a7928556813e3a5861a46584018c9497';
const app = express();
app.use(cors());
admin.initializeApp(functions.config().firebase);
// for breaking a chain of promises: https://stackoverflow.com/a/35503793/506182
function chainError(err) {
    return Promise.reject(err);
}
;
// app.get('/create_skus', (request, response)=>{
//   stripe.skus.create({
//     product: 'prod_CMNYGDL0qdAWhz',
//     attributes: {gigabytes: 15},
//     price: 9900,
//     currency: 'usd',
//     inventory: {type: 'infinite'}
//   })
//     .then((sku)=>{
//       console.log("forever plan SKU created...", sku)
//
//       return stripe.skus.create({
//         product: 'prod_CMNYGDL0qdAWhz',
//         attributes: {gigabytes: 0},
//         price: 19900,
//         currency: 'usd',
//         inventory: {type: 'infinite'}
//       })
//     })
//
//     .then((sku)=>{
//       console.log("unlimited plan SKU created...", sku)
//       response.json({success:true})
//
//     })
// })
app.post('/subscription', (request, response) => {
    let token = request.body.token;
    // response.json({'message':"Got token id : "+token.id})
    stripe.customers.list({ limit: 1, email: token.email }).
        then(result => {
        if (result.data.length === 0) {
            // create stripe customer
            return stripe.customers.create({
                email: token.email,
                source: token.id
            });
        }
        else {
            throw new Error('Customer already exists!');
        }
    }, chainError)
        .then(customer => {
        console.log("created customer : ", customer);
        // subscribe customer to plan
        return stripe.subscriptions.create({
            customer: customer.id,
            items: [{ plan: 'plan_CMNLIqYsNIZOq7' }],
        });
    }, chainError)
        .then(subscription => {
        console.log('created subscription : ', subscription);
        response.json({ success: true, subscription: subscription });
    }, chainError)
        .catch(error => {
        console.log("error creating customer : ", error);
        let err;
        if (!error.hasOwnProperty('statusCode')) {
            err = {};
            err['statusCode'] = 400;
            err['message'] = error.message;
        }
        else {
            err = error;
        }
        response.status(err.statusCode).json(err);
    });
    // save stripe customer id to db
});
app.post('/membership', (request, response) => {
    let token = request.body.token;
    let sku = request.body.sku;
    stripe.customers.list({ limit: 1, email: token.email }).
        // get or create customer...
        then(result => {
        if (result.data.length === 0) {
            // create stripe customer
            return stripe.customers.create({
                email: token.email,
                source: token
            });
        }
        else {
            console.log('Found existing customer : ', result.data[0]);
            return Promise.resolve(result.data[0]);
        }
    }, chainError)
        .then(customer => {
        console.log("received customer, now creating order for customer : ", customer);
        return stripe.orders.create({
            currency: 'usd',
            items: [
                {
                    type: 'sku',
                    parent: sku
                }
            ],
            customer: customer.id
        });
    }, chainError)
        .then(order => {
        console.log('created order, now paying : ', order);
        console.log('using token  : ', token);
        return stripe.orders.pay(order.id, {
            source: token.id,
        });
    }, chainError)
        .then(order => {
        console.log('paid order now fullfilling: ', order);
        return stripe.orders.update(order.id, {
            status: 'fulfilled'
        });
    }, chainError)
        .then(order => {
        console.log('Membership created : ', order);
        response.json({ success: true, order: order });
    }, chainError)
        .catch(error => {
        console.log("error purchasing membership : ", error);
        let err;
        if (!error.hasOwnProperty('statusCode')) {
            err = {};
            err['statusCode'] = 400;
            err['message'] = error.message;
        }
        else {
            err = error;
        }
        response.status(err.statusCode).json(err);
    });
    // save stripe ORDER id to db
});
//
// app.get('/order', (request, response)=>{
//
//   let sku = request.body.sku;
//   let customerId = request.body.customerId;
//
//   stripe.orders.create({
//       currency: 'usd',
//       items: [
//         {
//           type: 'sku',
//           parent: sku
//         }
//       ],
//       customer: customerId
//   })
//
//   .then((sku)=>{
//     console.log("unlimited plan SKU created...", sku)
//     response.json({success:true})
//
//   })
// })
app.post('/recordings/session', (request, response) => {
    // create OpenTok session
    // create token for client to connect
    // client should connect to session using token
    let opentok = new OpenTok(OPENTOK_API_KEY, OPENTOK_SECRET);
    // The session will the OpenTok Media Router:
    opentok.createSession({ mediaMode: "routed" }, (err, session) => {
        if (err) {
            console.log(err);
            response.status(500).json(err);
        }
        // save the sessionId
        //db.save('session', session.sessionId, done);
        // Generate a Token from a session object (returned from createSession)
        let token = opentok.generateToken(session.sessionId, { role: 'publisher' });
        // // Set some optioåns in a Token
        // token = session.generateToken({
        //   role :                   'moderator',
        //   expireTime :             (new Date().getTime() / 1000)+(7 * 24 * 60 * 60), // in one week
        //   data :                   'name=Johnny',
        //   initialLayoutClassList : ['focus']
        // });
        response.json({ success: true, token: token, sessionId: session.sessionId, apiKey: OPENTOK_API_KEY });
    });
});
app.post('recordings/start', (request, response) => {
    // once client is connected to a session, start archiving
    // save returned archive id to db
});
app.post('recordings/stop', (request, response) => {
    //stop recording
});
exports.api = functions.https.onRequest(app);
//
//# sourceMappingURL=index.js.map